// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: '#191919',
    secondary: 'white',
    tertiary: '#03A9FC',
    quaternary: '#CECECE',
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

export default class Presentation extends React.Component {


  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide transition={['zoom']} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            React Hooks
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            Why not code a presentation
          </Text>
        </Slide>


        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={6} textColor="primary" caps>
            Product Plan
          </Heading>
          <Heading size={1} textColor="secondary">
            DID THIS CHANGE
          </Heading>
          <Heading size={5} textColor="secondary">
            Heading 5
          </Heading>
          <Text size={6} textColor="secondary">
            Hi
          </Text>
        </Slide>


        <Slide transition={['fade']} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="secondary" caps>
            Standard List
          </Heading>
          <List>
            <ListItem>Item 1</ListItem>
            <ListItem>Item 2</ListItem>
            <ListItem>Item 3</ListItem>
            <ListItem>Item 4</ListItem>
          </List>
        </Slide>


        <Slide transition={['fade']} bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Quote>"Aliens are real"</Quote>
            <Cite>Bob Dylan</Cite>
          </BlockQuote>
        </Slide>


        <Slide transition={['slide']} bgColor="primary" textColor="secondary">
          <Heading size={3} textColor="tertiary">
            Lets give this a go
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 6
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            The New Paradigm
          </Heading>
          <Text size={2} textColor="tertiary">
            From Object Oriented Programming...   to Functional Programming
          </Text>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 8
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 9
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 10
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 11
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 12
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 13
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 14
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 15
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 16
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 17
          </Heading>
        </Slide>


        <Slide>
          <Heading size={6} textColor="secondary" caps>
            Slide 18
          </Heading>
        </Slide>
      </Deck>
    );
  }
}
